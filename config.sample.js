const config = {
  BITTREX_KEY: 'xxxxxxxx',
  BITTREX_SECRET: 'xxxxxxxx',
  KRAKEN_KEY: 'xxxxxxx',
  KRAKEN_SECRET: 'xxxxxxx',
};

export const BITTREX_KEY = 'xxxxxxxx';
export const BITTREX_SECRET = 'xxxxxxxx';
export const KRAKEN_KEY = 'xxxxxxxx';
export const KRAKEN_SECRET = 'xxxxxxxx';

export default config;
