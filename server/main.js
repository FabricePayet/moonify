/* global Meteor Accounts */

import exchangesAPI from './lib/exchanges';
import tradersAPI from './lib/traders';

const REFRESH_DELAY = 60000 * 5;

Meteor.startup(() => {
  exchangesAPI.updateDataForUsers();
  Meteor.setInterval(() => {
    exchangesAPI.updateDataForUsers();
  }, REFRESH_DELAY);
});

Accounts.onLogin(() => {
  exchangesAPI.updateDataForUser(Meteor.userId());
});

Accounts.onCreateUser((options, user) => {
  tradersAPI.addTrader(user);
  return user;
});

Meteor.publish('userData', function () {
  if (this.userId) {
    return Meteor.users.find({ _id: this.userId }, {
      fields: { exchanges: 1 }
    });
  } else {
    this.ready();
  }
});
