import { Meteor } from 'meteor/meteor'
import Fiber from 'fibers'
import async from 'async'

import krakenAPI from './api/kraken'
import bittrexAPI from './api/bittrex'
import balancesAPI from './balances'

_updateDataForUser = (userID) => {
  if (!userID) {
    return
  }
  console.log(`[ExchangeAPI] _updateDataForUser ${userID}`);
  async.parallel([
    (_cb) => {
      krakenAPI.updateDataForUser(userID, (err) => {
        console.error(`[kraken] err: ${err}`);
        _cb(err)
      })
    },
    (_cb) => {
      bittrexAPI.updateDataForUser(userID, (err) => {
        console.error(`[bittrex] err: ${err}`);
        _cb(err)
      })
    }], (err) => {
      console.log(`[ExchangeAPI] _updateDataForUser ${userID} completed`);
      if (err) {
        console.error(`Error happened when updating data for user ${userID}: ${err}`);
      }
      balancesAPI.updateSummaryForUser(userID);
  })
}

const exchangesAPI = {
  updateExchangeConfiguration: (exchange, key, secret) => {
    let update = {}
    update[`exchanges.${exchange}`] = {key, secret}
    Meteor.users.update(Meteor.userId(), {$set: update})
    let user = Meteor.users.findOne(Meteor.userId())
  },

  getExchangeConfigurationForUser: (exchange, userID) => {
    user = Meteor.users.findOne(userID)
    if (!user) {
      console.warn('getExchangeConfigurationForUser user not found', userID);
      return
    }
    if (!user.exchanges) {
      return null
    }
    return user.exchanges[exchange]
  },

  invalidExchangeConfigurationForUser: (exchange, userID) => {
    Fiber(function() {
      let update = {}
      update[`exchanges.${exchange}`] = {invalid: true}
      Meteor.users.update(userID, {$set: update})
    }).run()
  },

  updateDataForUsers() {
    let users = Meteor.users.find().fetch();
    for ({ _id } in users) {
      _updateDataForUser(_id)
    }
  },

  updateDataForUser(userID) {
    _updateDataForUser(userID)
  },

  updateDataForUsername(username) {
    const user = Meteor.users.findOne({ username });
    if (!user) {
      return;
    }
    _updateDataForUser(user._id);
  },
};

export default exchangesAPI;
