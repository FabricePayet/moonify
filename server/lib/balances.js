import Fiber from 'fibers';
import async from 'async'

const Balances = new Mongo.Collection('balances');

_getPortefolioValue = () => {
  let exchangeBalances = Balances.find().fetch();
  return _.reduce(exchangeBalances, (memo, exchange) => {
    if (!exchange) {
      return memo + 0
    }
    return memo + exchange.total_value
  }, 0)
}

const balancesAPI = {
  updateBalance(userID, exchange, exchangeBalance) {
    console.log('updating balance for user', exchange, exchangeBalance);
    Fiber(function() {
      let update = {}
      update[exchange] = {
        balance: exchangeBalance,
        total_value: _.reduce(exchangeBalance, (memo, balance) => {
          return memo + balance.value
        }, 0)
      }
      Balances.update(userID, {
        $set: update
      })
    }).run()
  },

  updateSummaryForUser: (userID) => {
    console.info('Update balance summary for user:', userID);
    let userBalance = Balances.findOne(userID)
    if (!userBalance) {
      return console.error('error when updating summary balance for user', userID);
    }
    // TODO: find an elegant way to get all exchanges
    let exchangeBalances = [userBalance.bittrex, userBalance.kraken]
    let summary = {}
    let portfolioValue = _.reduce(exchangeBalances, (memo, exch) => exch.total_value + memo, 0)
    for (let exchange of exchangeBalances) {
      if (!exchange.balance) {
        return
      }
      _.each(exchange.balance, ({currency, balance, value}) => {
        // calculer en percentage ici
        let percent = (value / portfolioValue) * 100
        if (summary[currency]) {
          summary[currency] += percent
        } else {
          summary[currency] = percent
        }
      })
    }
    Balances.update(userID, {
      $set: {summary}
    }, {upsert: true})
  }
}

Meteor.publish('balances', () => Balances.find({}, {fields: {'summary': 1}}))
// Meteor.publish('balances', () => Balances.find({}))

export default balancesAPI;
