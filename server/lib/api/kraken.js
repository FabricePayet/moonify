import Kraken from 'kraken-wrapper';
import async from 'async';
import _ from 'underscore';
import { Mongo } from 'meteor/mongo';

import { KRAKEN_KEY, KRAKEN_SECRET } from '../../../config';

import tradersAPI from '../traders'
import balancesAPI from '../balances'
import exchangesAPI from '../exchanges'

const dbOrders = new Mongo.Collection('orders');
const dbTickers = new Mongo.Collection('tickers');

const _handleInvalidAPIKey = (userID, callback) => {
  // TODO: Handle callback here
  exchangesAPI.invalidExchangeConfigurationForUser('kraken', userID)
  callback(new Meteor.Error(''))
}

const _getEuroPairFromDevise = (devise) => {
  return `${devise}ZEUR`
}

_getDeviseFromPair = (pair) => {
  return pair.substring(0, 4)
}

_getUnitValueForPair = (pair) => {
  if (pair == 'ZEURZEUR') {
    return pair;
  }
  let euroPair = _getEuroPairFromDevise(pair)
  let value = dbTickers.findOne(euroPair)
  if (!value) {
    console.error(`getCurrentValueForPair: pair not found ${pair}`);
    return 0;
  }
  return value.a[0]
}

_formatKrakenOpenedOrders = (openOrders) => {
  return openOrders
}

const formatKrakenResult = ({ balance, currency }, callback) => {
  callback(null, {
    balance,
    currency,
    value: balance,
  });
};

updateKrakenBalanceForUser = (userID, callback) => {
  console.log(`[KRAKEN] updateKrakenBalanceForUser ${userID}`);
  let config = exchangesAPI.getExchangeConfigurationForUser('kraken', userID)
  if (!config) {
    return callback(new Meteor.Error(`[KRAKEN] API key not configured for user: ${userID}`))
  }
  let {key, secret, invalid} = config;
  if (!key || !secret || invalid) {
    return callback(new Meteor.Error(`[KRAKEN] Key invalid for user: ${userID}`))
  }
  let krakenConfig = new Kraken(key, secret);
  krakenConfig.getBalance().then(({ error, result }) => {
    if (!_.isEmpty(error)) {
      // switch case doesnt work here. I don't know why
      if (error === 'EAPI:Invalid nonce') {
        return callback(new Meteor.Error(`[KRAKEN] performance issue when updating portfolio for user: ${userID}`))
      } else if (error === 'EAPI:Invalid key') {
        _handleInvalidAPIKey(userID);
        process.nextTick(() => {
          return callback(new Meteor.Error(`[KRAKEN] Wrong API key for user: ${userID}`))
        })
      } else {
        return callback(new Meteor.Error(`[KRAKEN] unknown error when updating portfolio for user: ${userID} : ${error}`))
      }
    } else {
      if (result) {
        let resultArray = _.keys(result).map((r) => ({
          currency: r,
          balance: parseFloat(result[r]),
        }));
        async.map(resultArray, formatKrakenResult, (err, results) => {
          if (err) {
            callback(err)
          } else {
            balancesAPI.updateBalance(userID, 'kraken', results);
            callback(null);
          }
        })
      } else {
        callback(new Meteor.Error('[Kraken] No results'))
      }
    }
  }).catch((error) => {
    console.error(error);
  });
};

const getTickerInformation = (callback) => {
  const krakenConfig = new Kraken(KRAKEN_KEY, KRAKEN_SECRET);
  const pair = 'XETHZEUR, XXBTZEUR, XZECZEUR, XXRPZEUR, XXMRZEUR';
  krakenConfig.getTickerInformation({ pair }).then(({ error, result }) => {
    if (error) {
      callback(error);
    } else {
      _.each(result, (val, key) => {
        dbTickers.update(key, val, { upsert: true });
      });
      callback();
    }
  }).catch((error) => {
    callback(error);
  });
};

updateTradesForUser = (userID, callback) => {
  let config = exchangesAPI.getExchangeConfigurationForUser('kraken', userID)
  if (!config) {
    return callback(new Meteor.Error(`[KRAKEN] Kraken API key not configured for user ${userID}`))
  }
  let {key, secret, invalid} = config;
  if (!key || !secret || invalid) {
    return callback(new Meteor.Error(`[KRAKEN] Key invalid for user ${userID}`))
  }
  let krakenConfig = new Kraken(key, secret);
  krakenConfig.getOpenOrders().then(({error, result}) => {
    if (!_.isEmpty(error)) {
      // switch case doesnt work here. I don't know why
      if (error === 'EAPI:Invalid nonce') {
        callback(new Meteor.Error(`[KRAKEN] Kraken performance issue: Invalid nonce for user ${userID}`))
      } else if (error === 'EAPI:Invalid key') {
        _handleInvalidAPIKey(userID, callback);
        process.nextTick(() => {
          return callback(new Meteor.Error(`[KRAKEN] Configuration is invalid for user ${userID}`))
        })
      } else {
        callback(new Meteor.Error(`[KRAKEN] Unknown error for user ${userID}: ${error}`))
      }
    } else {
      if (!result) {
        return callback(new Meteor.Error(`[KRAKEN] Orders result not found for user ${userID}`))
      }
      if (result.open) {
        let openOrdersFormatted = _formatKrakenOpenedOrders(result.open)
        tradersAPI.updateOpenOrders(userID, openOrdersFormatted)
        callback()
      }
    }
  }).catch((error) => {
    callback(error)
  });
};

const krakenAPI = {
  updateDataForUser: (userID, callback) => {
    console.log(`[KRAKEN] updateDataForUser ${userID}`);
    async.parallel([
      (_cb) => updateKrakenBalanceForUser(userID, _cb),
      (_cb) => updateTradesForUser(userID, _cb)
    ], callback)
  }
};

Meteor.publish('tickers', () => dbTickers.find())

export default krakenAPI;
