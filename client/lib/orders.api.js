const Tickers = new Mongo.Collection('tickers');

orderClientAPI = {
  updateDataForCurrentUser: () => {
    Meteor.call('updateDataForCurrentUser')
  },

  updateExchangeConfiguration: (exchange, key, secret) => {
    Meteor.call('updateExchangeConfiguration', exchange, key, secret)
  },

  evaluatePortfolio: (balance) => {
    // HACK: _.mapObject is not defined! Shit! Use another solution
    portefolioEUR = {}
    let portefolio = _.each(balance, (value, key) => {
      pairKey = `${key}ZEUR`;
      tickerValue = Tickers.findOne(pairKey);
      if (!tickerValue) {
        portefolioEUR[key.substring(1)] = value
        return
      }
      portefolioEUR[key.substring(1)] = tickerValue.a[0] * value
    })
    return portefolioEUR;
  },

  getTickers: () => {
    tickers = Tickers.find().fetch()
    tickersInfo = []
    _.map(tickers, (val, key) => tickersInfo.push({
      asset: val._id.substring(1,4),
      value: val.a[0]
    }))
    return tickersInfo
  }
}


export default orderClientAPI;
