Meteor.subscribe('userData');

const userAPI = {
  isCurrentUser: (username) => {
    let profile = Meteor.user()
    if (!profile) {
      return false
    }
    return (username == profile.username)
  },

  getProfileForCurrentUser: () => {
    let profile = Meteor.user()
    return profile
  }
}

export default userAPI;
