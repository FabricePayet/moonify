import userAPI from './lib/users.api'

FlowRouter.route('/', {
  name: 'Homepage',
  action() {
    BlazeLayout.render('App', {template: 'Homepage'});
  }
});

FlowRouter.route('/profile/edit', {
  name: 'EditProfile',
  action() {
    BlazeLayout.render('App', {template: 'EditProfile'});
  }
});

FlowRouter.route('/user/:username', {
  name: 'TraderProfile',
  action(params) {
    BlazeLayout.render('App', {
      template: 'TraderProfile'
    });
  }
});

FlowRouter.route('/login', {
  name: 'Login',
  action: (params) => {
    BlazeLayout.render('App', {template: 'Login'});
  }
});

FlowRouter.route('/signup', {
  name: 'Signup',
  action: (params) => {
    BlazeLayout.render('App', {template: 'Signup'});
  }
});

FlowRouter.route('/best-traders', {
  name: 'BestTraders',
  action: (params) => {
    BlazeLayout.render('App', {template: 'BestTraders'});
  }
});

FlowRouter.notFound = {
    action: function() {
      BlazeLayout.render('App', {template: 'NotFound'});
    }
};
