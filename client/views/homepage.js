Template.Homepage.onCreated(function() {
  this.postsResults = new ReactiveVar(null);
  $.ajax({
    url: 'https://randomuser.me/api/?results=8',
    dataType: 'json',
    success: ({info, results}) => {
      if (results) {
        console.log('results', results);
        this.postsResults.set(results)
      }
    }
  });
})

Template.Homepage.helpers({
  recentPosts: () => {
    return Template.instance().postsResults.get()
  }
})

Template.Homepage.events({
  'click [data-action="go-profile"]': function(e, tpl) {
    FlowRouter.redirect(`/user/${this.login.username}`);
  }
})
