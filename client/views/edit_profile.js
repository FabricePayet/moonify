import orderAPI from '../lib/orders.api'

Template.EditProfile.helpers({
  userEmail: function() {
    return Meteor.user().emails[0].address
  },

  getUsername: function() {
    return Meteor.user().username
  },

  krakenKey: () => {
    let user = Meteor.users.findOne()
    if (!user.exchanges) {
      return
    }
    let {kraken} = user.exchanges
    if (!kraken) {
      return
    }
    return kraken.key
  },

  krakenSecret: () => {
    let user = Meteor.users.findOne()
    if (!user.exchanges) {
      return
    }
    let {kraken} = user.exchanges
    if (!kraken) {
      return
    }
    return kraken.secret
  },

  bittrexKey: () => {
    let user = Meteor.users.findOne()
    if (!user.exchanges) {
      return
    }
    let {bittrex} = user.exchanges
    if (!bittrex) {
      return
    }
    return bittrex.key
  },

  bittrexSecret: () => {
    let user = Meteor.users.findOne()
    if (!user.exchanges) {
      return
    }
    let {bittrex} = user.exchanges
    if (!bittrex) {
      return
    }
    return bittrex.secret
  }
})

Template.EditProfile.events({
  'click [data-action="save-profile"]': (e, tpl) => {
    let krakenKey = tpl.$('#kraken_api_key').val();
    let krakenSecret = tpl.$('#kraken_api_secret').val();
    if(krakenKey && krakenSecret) {
      orderAPI.updateExchangeConfiguration('kraken', krakenKey, krakenSecret);
    }

    let bittrexKey = tpl.$('#bittrex_api_key').val();
    let bittrexSecret = tpl.$('#bittrex_api_secret').val();
    if(bittrexKey && bittrexSecret) {
      orderAPI.updateExchangeConfiguration('bittrex', bittrexKey, bittrexSecret);
    }
  }
})
